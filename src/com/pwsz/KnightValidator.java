package com.pwsz;

public interface KnightValidator {
    boolean validate(Knight knight);
}
