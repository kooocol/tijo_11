# Strategia (wzorzec projektowy) #

**Strategia** - czynnościowy wzorzec projektowy, który definiuje rodzinę wymiennych algorytmów
i kapsułkuje je w postaci klas. Umożliwia wymienne stosowanie każdego z nich w trakcie działania
aplikacji niezależnie od korzystających z nich użytkowników. 


# Struktura wzorca #
We wzorcu Strategia definiujemy wspólny interfejs dla wszystkich obsługiwanych algorytmów
i zawierający wszystkie dozwolone operacje. Następnie implementujemy go w poszczególnych klasach
dostarczających konkretne algorytmy. Dodatkowo, we wzorcu wyróżniamy także klienta korzystającego
z algorytmów. Posiada on referencję do aktualnie używanej strategii oraz metodę ustawStrategie(), która pozwala ją zmienić.

**Elementy wzorca:**

Strategia - interfejs definiujący operacje, które muszą obsługiwać wszystkie dostępne algorytmy. Zakładamy, że wszyscy klienci
zainteresowani wykorzystaniem algorytmów będą używać właśnie tego interfejsu.

Konkretna strategia - implementuje określony algorytm zgodnie ze zdefiniowanym interfejsem.

Klient - użytkownik rodziny algorytmów posiadający referencję do obiektu Strategia.
Istotne jest, że obiekty Klient oraz Strategia współpracują ze sobą w celu wykonania określonego zadania. Klient wykonuje wszystkie ogólne
zadania i nadzoruje przepływ sterowania, zaś strategie implementują te części zadania, które można wymieniać. 


# Przykład zastosowania #
W podanym przykładzie wzorzec strategii realizują interfejs KnightValidator oraz implementujące go klasy OldKnightsValidator oraz MaleKnightsValidator,
które określają, czy wskazany obiekt klasy Knight spełnia kryterium dotyczące wieku lub imienia. Klasa Arena wykorzystuje przekazany w czasie działania
programu obiekt KnightValidator do kontrolowania przyjmowanych rycerzy. 

# Konsekwencje stosowania #

**Zalety:**

1. redukcja lub usunięcie wyrażeń warunkowych,
2. prosta struktura kodu po przeniesieniu odmian algorytmu do określonych klas,
3. możliwość zmiany algorytmu w czasie działania programu (dzięki zastosowaniu kompozycji usuwającej powiązanie między algorytmem a miejscem jego użycia),
4. łatwe testowanie klasy klienta i klas strategii,
5. łatwa analiza kodu, gdy mamy do czynienia z dużą ilością algorytmów.
	
**Wady:**

1. złożona konstrukcja kodu (więcej klas),
2. złożony sposób pobierania danych z klasy kontekstu.
	